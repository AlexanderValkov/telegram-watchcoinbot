hlp = """
/top N to list top N currencies (default N = 10, max N = 100)
/price Dogecoin - show the price of Dogecoin (substitute last for any other valid currency)
"""

not_defined = """
not defined
"""

canceled = """
Canceled
"""

coin_not_valid = """
This doesn't seem to be a valid coin name
"""

coin_name_cant_be_empty = """
Coin name can't be empty
"""
