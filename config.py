import os
token = os.environ.get('TELEGRAM_TOKEN')
app_fqdn = os.environ.get('APP_FQDN')
port = int(os.environ.get('PORT', 5000))
heroku = int(os.environ.get('HEROKU', 0))
