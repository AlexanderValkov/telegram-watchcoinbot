import urllib.request
import json

MAX_COINS = 1000


def get_cryptonames(n):
    """returns 4 dicts: ids, names, symbols and name_data (contains all of it with rank as an index)
    Keys are corresponding written names, values are ranks
    Top n resulst are returned.
    """
    contents = urllib.request.urlopen("https://api.coinmarketcap.com/v1/ticker/?limit=%s" % (n,)).read().decode('utf-8')

    data = json.loads(contents)

    # initialize empty dicts
    ids = dict()
    names = dict()
    symbols = dict()
    name_data = list()
    name_data.append(0)

    # populate dicts
    for i in range(len(data)):
        ids[data[i]['id'].lower()] = i + 1
        names[data[i]['name'].lower()] = i + 1
        symbols[data[i]['symbol'].lower()] = i + 1
        name_data.append({'id': data[i]['id'], 'name': data[i]['name'], 'symbol': data[i]['symbol']})

    return (ids, names, symbols, name_data)


(ids, names, symbols, name_data) = get_cryptonames(MAX_COINS)


def get_coin_data(ids=ids, names=names, symbols=symbols, name_data=name_data, id_="", name="", symbol=""):
    """returns coin ifno for specified id, name or symbol"""
    result = ""
    rank = 0
    try:
        if id_:
            rank = ids[id_]
        elif name:
            rank = names[name]
        elif symbol:
            rank = symbols[symbol]
    except:
        pass

    try:
        result = {'sid': rank, 'id': name_data[rank]['id'], 'name': name_data[rank]['name'], 'symbol': name_data[rank]['symbol']}
    except:
        result = {'sid': 0, 'id': 0, 'name': 0, 'symbol': 0}

    return result


def search_coin_data(s):
    """returns coin info if specified argument matches id, name or symbol"""
    s = s.lower()
    d = get_coin_data(id_=s)
    if not d['sid']:
        d = get_coin_data(name=s)
        if not d['sid']:
            d = get_coin_data(symbol=s)
    if not d['sid']:
        return 0
    return d


def get_coin_usd_rate(cid):
    url = "https://api.coinmarketcap.com/v1/ticker/%s/" % (cid,)
    contents = urllib.request.urlopen(url).read().decode('utf-8')

    data = json.loads(contents)

    try:
        return data[0]['price_usd']
    except:
        return 0


def get_top_n_coins(n):
    contents = urllib.request.urlopen("https://api.coinmarketcap.com/v1/ticker/?limit=%s" % (n,)).read().decode('utf-8')

    result = json.loads(contents)

    # converting resulst into list of dicts
    lst = list()
    for i in range(len(result)):
        lst.append({'id': result[i]['id'], 'name': result[i]['name'], 'symbol': result[i]['symbol']})
    return lst
