from telebot.types import ReplyKeyboardMarkup as RKM, KeyboardButton as KB, ReplyKeyboardRemove as RKR

kb_remove = RKR()

kb_welcome = RKM(resize_keyboard=True)
kb_welcome.add(KB(text="/help"))
kb_welcome.add(KB(text="/top"), KB(text="/price"))
