import telebot
from telebot.types import ReplyKeyboardMarkup as RKM, KeyboardButton as KB, ReplyKeyboardRemove as RKR
import config
from flask import Flask, request
import logging
from datetime import datetime, timedelta
from keyboards_reply import *
import msg_templates
from cmcapi import *


bot = telebot.TeleBot(config.token)
server = Flask(__name__)

logger = telebot.logger
logger.setLevel(logging.INFO)


@bot.message_handler(commands=['start', 'help'])
def help(message):
    logger.info("%s: %s" % (message.chat.id, message.text))
    bot.send_message(message.chat.id, msg_templates.hlp, parse_mode="Markdown", reply_markup=kb_welcome)


@bot.message_handler(commands=['top'])
def top(message):
    logger.info("%s: %s" % (message.chat.id, message.text))
    top_n_display(message)


@bot.message_handler(commands=['price', 'Price', 'p', 'P'])
def rate(message):
    logger.info("%s: %s" % (message.chat.id, message.text))
    message.text = " ".join(message.text.split()[1:])  # to pass it to show_coin_rate, we need to get rid of the command

    if not message.text:
        message.text = "/top 10"
        top_n_display(message)
        return

    show_coin_rate(message)


@bot.message_handler(content_types=["text"])
def text(message):
    logger.info("%s: %s" % (message.chat.id, message.text))
    coin_data = is_it_coin(message)

    # expected default
    if not coin_data:
        bot.send_message(message.chat.id, msg_templates.not_defined, parse_mode="Markdown", reply_markup=kb_welcome)
        return

    # in case if message actually a coin
    text = "Sorry, couldn't fetch price for %s" % message.text
    price_usd = get_coin_usd_rate(coin_data['id'])
    if price_usd:
        text = "1 %s (%s) is currently worth $%s" % (coin_data['name'], coin_data['symbol'], price_usd)
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=kb_welcome)


def cancel(message):
    logger.info("%s: %s" % (message.chat.id, message.text))
    bot.send_message(message.chat.id, msg_templates.canceled, parse_mode="Markdown", reply_markup=kb_welcome)


def show_coin_rate(message):
    try:
        coin = message.text
    except:
        bot.send_message(message.chat.id, msg_templates.not_defined, parse_mode="Markdown", reply_markup=kb_welcome)
        return

    if not coin:
        bot.send_message(message.chat.id, msg_templates.coin_name_cant_be_empty, parse_mode="Markdown", reply_markup=kb_welcome)
        return

    if coin == "/cancel":
        cancel(message)
        return

    coin_data = search_coin_data(coin)
    if not coin_data:
        bot.send_message(message.chat.id, msg_templates.coin_not_valid, parse_mode="Markdown", reply_markup=kb_welcome)
        return

    text = "Sorry, couldn't fetch price for %s" % coin
    price_usd = get_coin_usd_rate(coin_data['id'])
    if price_usd:
        text = "1 %s (%s) is currently worth $%s" % (coin_data['name'], coin_data['symbol'], price_usd)
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=kb_welcome)


def top_n_display(message):
    try:
        n = int(message.text.split()[1])    # check if user provided correct n
    except:
        n = 10                              # assign default if not

    if n > 100:
        n = 100

    topn = get_top_n_coins(n)

    msg = "Top %s currencies:\n" % (n, )
    kb_top_n_coins = RKM(resize_keyboard=True)
    kb_top_n_coins.add(KB("/cancel"))
    for i in range(0, n, 2):
        try:
            kb_top_n_coins.add(KB(text=topn[i]['name']), KB(text=topn[i + 1]['name']))
            msg += "%s. %s\n" % (i + 1, topn[i]['name'])
            msg += "%s. %s\n" % (i + 2, topn[i + 1]['name'])
        except:
            kb_top_n_coins.add(KB(text=topn[i]['name']))
            msg += "%s. %s\n" % (i + 1, topn[i]['name'])

    msg = msg + "Choose one from a keyboard (Or type it manually)"

    sendmessage = bot.send_message(message.chat.id, msg, parse_mode="Markdown", reply_markup=kb_top_n_coins)
    bot.register_next_step_handler(sendmessage, show_coin_rate)
    return


def is_it_coin(message):
    try:
        coin = message.text
    except:
        return False

    if not coin:
        return False

    if coin == "/cancel":
        return False

    coin_data = search_coin_data(coin)
    if not coin_data:
        return False

    return coin_data


@server.route('/' + config.token, methods=['POST'])
def getMessage():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route("/")
def webhook():
    bot.remove_webhook()
    logger.info("webhook removed")
    bot.set_webhook(url=config.app_fqdn + config.token)
    logger.info("webhook set")
    return "!", 200


if __name__ == "__main__":
    if config.heroku:
        server.run(host="0.0.0.0", port=config.port)
    else:
        bot.polling(none_stop=True)
